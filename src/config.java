import java.sql.*;

public class config {
    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost:3306/lesson26";
        String name = "root";
        String password = "Root@12345";

        try {
            Connection connection = DriverManager.getConnection(url, name, password);
            
            createStudentTeacherGroupTable(connection);
            createGroupTable(connection);
            createGroupRowInfoTable(connection);  
            createTeacherTable(connection);
            createTeacherRowInfoTable(connection);
            createStudentTable(connection);
            createStudentRowInfoTable(connection);

            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }

    private static void createStudentRowInfoTable(Connection connection) throws SQLException {
        String createStudentRowInfoQuery = "CREATE TABLE IF NOT EXISTS student_row_info (" +
                "id INT PRIMARY," +
                "create_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
                "update_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP," +
                "status INT DEFAULT 1" +
                "FOREIGN KEY(id) REFERENCES student_row_info_id(student_row_info_id)" +
                ");";

        try (Statement statement = connection.createStatement()) {
            statement.execute(createStudentRowInfoQuery);
        }
    }

    private static void createStudentTable(Connection connection)  throws SQLException {
        String createStudentQuery="CREATE TABLE IF NOT EXISTS Student(" +
                "id INT PRIMARY KEY," +
                "name VARCHAR(255)," +
                "surname VARCHAR(255)," +
                "major VARCHAR(255)" +
                "fee DOUBLE," +
                "start_date DATE," +
                "leave_date DATE," +
                "student_row_info_id INT," +
                "FOREIGN KEY(student_row_info_id) REFERENCES student_teacher_group(student_id)" +
                ");";

        try(Statement statement=connection.createStatement()) {
            statement.execute(createStudentQuery);
        }
    }

    private static void createTeacherRowInfoTable(Connection connection) throws SQLException  {
        String createTeacherRowInfoQuery = "CREATE TABLE IF NOT EXISTS teacher_row_info (" +
                "id INT PRIMARY," +
                "create_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
                "update_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP," +
                "status INT DEFAULT 1" +
                "FOREIGN KEY(id) REFERENCES teacher_row_info_id(teacher_row_info_id)" +
                ");";

        try (Statement statement = connection.createStatement()) {
            statement.execute(createTeacherRowInfoQuery);
        }
    }

    private static void createTeacherTable(Connection connection)  throws SQLException {
        String createTeacherQuery="CREATE TABLE IF NOT EXISTS Teacher(" +
                "id INT PRIMARY KEY," +
                "name VARCHAR(255)," +
                "surname VARCHAR(255)," +
                "profession VARCHAR(255)" +
                "salary DOUBLE," +
                "start_date DATE," +
                "leave_date DATE," +
                "teacher_row_info_id INT," +
                "FOREIGN KEY(teacher_row_info_id) REFERENCES student_teacher_group(teacher_id)" +
                ");";

        try(Statement statement=connection.createStatement()) {
            statement.execute(createTeacherQuery);
        }
        
    }

    private static void createGroupRowInfoTable(Connection connection) throws SQLException  {
        String createGroupRowInfoQuery = "CREATE TABLE IF NOT EXISTS group_row_info (" +
                "id INT PRIMARY," +
                "create_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
                "update_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP," +
                "status INT DEFAULT 1" +
                "FOREIGN KEY(id) REFERENCES group(group_row_info_id)" +
                ");";

        try (Statement statement = connection.createStatement()) {
            statement.execute(createGroupRowInfoQuery);
        }
    }

    private static void createGroupTable(Connection connection)  throws SQLException {
        String createGroupQuery="CREATE TABLE IF NOT EXISTS Group(" +
                "id INT PRIMARY KEY," +
                "name VARCHAR(255)," +
                "group_row_info_id INT," +
                "FOREIGN KEY(id) REFERENCES student_teacher_group(group_id)" +
                "FOREIGN KEY(group_row_info_id) REFERENCES group_row_info(id)" +
                ");";

        try(Statement statement=connection.createStatement()) {
            statement.execute(createGroupQuery);
        }
        
    }

    private static void createStudentTeacherGroupTable(Connection connection)  throws SQLException {
        String createStudentQuery="CREATE TABLE IF NOT EXISTS Student(" +
                "id INT PRIMARY KEY," +
                "group_id INT PRIMARY KEY," +
                "student_id INT PRIMARY KEY," +
                "teacher_id INT PRIMARY KEY," +
                ");";

        try(Statement statement=connection.createStatement()) {
            statement.execute(createStudentQuery);
        }
        
    }


}    